def my_max(number_list):
    max_num = number_list[0]
    for number in number_list:
        if max_num < number:
            max_num = number
    return max_num
