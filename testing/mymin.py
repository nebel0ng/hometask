def my_min(number_list):
    min_num = number_list[0]
    for number in number_list:
        if min_num > number:
            min_num = number
    return min_num
