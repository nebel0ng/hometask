def my_product(number_list):
    product = 1
    for number in number_list:
        try:
            product *= number
        except:
            print('Limit exceeded')
            break
    return product
