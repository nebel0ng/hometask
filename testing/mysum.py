def my_sum(number_list):
    total = 0
    for number in number_list:
        total += number
    return total
