from mymax import my_max
from tz3 import num_list


def test_max():
    assert my_max(num_list) == max(num_list)
