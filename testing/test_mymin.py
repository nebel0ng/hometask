from mymin import my_min
from tz3 import num_list


def test_min():
    assert my_min(num_list) == min(num_list)
