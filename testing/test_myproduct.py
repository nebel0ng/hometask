from myproduct import my_product
from tz3 import num_list
from math import prod


def test_myproduct():
    assert my_product(num_list) == prod(num_list)

