from mysum import my_sum
from tz3 import num_list


def test_mysum():
    assert my_sum(num_list) == sum(num_list)

