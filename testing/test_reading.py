from tz3 import reading
import pytest


def test_reading():
    with pytest.raises(ValueError):
        reading('error_file.txt',  'r', 'UTF-8')