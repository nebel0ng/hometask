from timing import timing
from tz3 import num_list


def test_timing():
    assert timing(num_list*500) > timing(num_list[:5])
