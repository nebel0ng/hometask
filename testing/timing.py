from time import time
from mymin import my_min
from mymax import my_max
from myproduct import my_product
from mysum import my_sum
from tz3 import num_list


def timing(number_list):
    start = time()
    mmax = my_max(number_list)
    mmin = my_min(number_list)
    msum = my_sum(number_list)
    prod = my_product(number_list)
    end = time()
    return end - start

