from mymin import my_min
from mymax import my_max
from myproduct import my_product
from mysum import my_sum


def reading(file_name, mode, encoding):
    with open(file_name, mode, encoding=encoding) as f:
        file = f.read()
        number_list = file.split()
        for i in range(len(number_list)):
            number_list[i] = int(number_list[i])
    return number_list


num_list = reading('file.txt', 'r', 'UTF-8')

mmax = my_max(num_list)
mmin = my_min(num_list)
msum = my_sum(num_list)
prod = my_product(num_list)
#
# print(f'Максимум: {mmax}')
# print(f'Минимум: {mmin}')
# print(f'Сумма: {msum}')
# print(f'Произведение: {prod}')
